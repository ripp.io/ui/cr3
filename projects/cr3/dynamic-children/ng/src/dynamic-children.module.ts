import { Injector, NgModule } from '@angular/core';
import { LoadChildrenCallback } from '@angular/router';

let injector: Injector;

@NgModule()
export class DynamicChildrenModule {
  constructor(inj: Injector) {
    injector = inj;
  }
}

// Provide an inline function
export function inlineSwitch(cb: (injector: Injector) => Promise<LoadChildrenCallback>): LoadChildrenCallback {
  return async () => await cb(injector);
}

// Provide a service to handle the decision
export interface DynamicChildrenService {
  handleRoute(components: { [key: string]: LoadChildrenCallback }): LoadChildrenCallback | Promise<LoadChildrenCallback>;
}

export function serviceSwitch(
  Service: new (...args: any[]) => DynamicChildrenService, components: { [key: string]: LoadChildrenCallback }
): LoadChildrenCallback {
  return async () => (await injector.get(Service).determineComponent(components))();
}
