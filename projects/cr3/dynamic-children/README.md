# Dynamic Children (Angular Only)

Dynamically swap out `loadChildren` components to load.

See [ripp.io/cr3/dynamic-children](https://ripp.io/cr3/dynamic-children) for more friendly and interactive documentation.

#### Import Module
```typescript
import { DynamicChildrenModule } from '@cr3/dynamic-children';

@NgModule({
  ...
  imports: [
    ...
    DynamicChildrenModule,
  ]
```

Then choose between using [your own service](#service) or an [inline function](inline).
Recommended to use [your own service](#service) for cleanliness and easily access other dependencies such as RBAC and other complex logic.

## Service

#### Key your routes
```typescript
import { serviceSwitch } from '@cr3/dynamic-children';


const routes: Routes = [
  {                         // (1)        // (2)
    path: '', loadChildren: serviceSwitch(MyFancyService, {
      // (3)
      unauthenticated: async () => (await import('./home/home-public/home-public.module')).HomePublicModule,
      authenticated:   async () => (await import('./home/home-secure/home-secure.module')).HomeSecureModule
    })
  },
];
```

1) Use the `serviceSwitch`, which will inject the provided service and call it's `determineComponent` method with the map of `string: loadChildren callback` 
2) Give the class of the service (that implements `DynamicChildrenService`) to inject and call
3) Provide a map of which the service can respond which callback to utilize

#### Build Your Service
```typescript
import { LoadChildren, LoadChildrenCallback } from '@angular/router';
import { DynamicChildrenService } from '@cr3/dynamic-children';

@Injectable({providedIn: 'root' })
export class MyFancyService implements DynamicChildrenService { // (1)
  constructor(private session: SomeSweetSessionService) { }

  // (2)
  determineComponent(components: { [key: string]: LoadChildrenCallback }): LoadChildrenCallback {
    // (3)
    return session.isAuthenicated() ? components.authenticated : components.unauthenticated;
  }
}
```

1) Implement `DynamicChildrenService` for (2)
2) Implement the `determineComponent` method
3) Determine the proper component to load and return the loadChildrenCallback


## Inline

#### Build Routes

```typescript
import { inlineSwitch } from '@cr3/dynamic-children';
import { Injector } from '@angular/core';


const routes: Routes = [
  {                         // (1)        // (2)
    path: '', loadChildren: inlineSwitch((injector: Injector) => {
      // (3)
      return async () => (await import('./home/home-public/home-public.module')).HomePublicModule
    })
  },
];
```

1) Use the `inlineSwitch`
2) Injector is supplied if needing to inject any services
3) Return a loadChildrenCallback to be loaded into the route
