export class Cache<T> {
  static MILLISECOND     = 1;
  static SECONDS         = 1000;
  static MINUTES         = 60000;
  static TIME_MULTIPLIER = Cache.SECONDS;

  static DEFAULT_TTL            = 60;
  static DEFAULT_CLEAN_INTERVAL = 180;

  private cache: { [key: string]: { ex: number, obj: T } } = {};
  private readonly defaultTtl: number;
  private readonly timeMultiplier: number;
  private cleanInterval?: number;

  constructor(defaultTtl?: number, cleanInterval?: number, timeMultiplier?: number) {
    this.defaultTtl     = defaultTtl || Cache.DEFAULT_TTL;
    this.timeMultiplier = timeMultiplier || Cache.TIME_MULTIPLIER;

    if (cleanInterval !== 0 || Cache.DEFAULT_CLEAN_INTERVAL) {
      this.startCleanInterval(cleanInterval || Cache.DEFAULT_CLEAN_INTERVAL);
    }
  }

  public set(key: string, obj: T, ttl?: number): void {
    const expire = new Date();
    expire.setMilliseconds(expire.getMilliseconds() + ((ttl || this.defaultTtl) * this.timeMultiplier));
    this.cache[key] = {ex: expire.getTime(), obj};
  }

  public get(key: string): T | null {
    const cache = this.cache[key];

    if (cache && Date.now() > cache.ex) {
      delete this.cache[key];
      return null;
    }

    return cache?.obj;
  }

  public remove(key: string): void {
    delete this.cache[key];
  }

  public clean(): void {
    Object.keys(this.cache).forEach(k => this.get(k));
  }

  public clear(): void {
    this.cache = {};
  }

  public startCleanInterval(interval: number): void {
    this.cleanInterval = setInterval(() => this.clean(), interval * this.timeMultiplier) as any;
  }

  public stopCleanInterval(): void {
    clearInterval(this.cleanInterval);
    delete this.cleanInterval;
  }
}
