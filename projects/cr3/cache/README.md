# Cache

In-memory cache with:
* Expiry per object with global default
* Cleanup routine
* Sync across tabs or session (local storage)
  * Event on cross-tab update

See [ripp.io/cr3/dynamic-children](https://ripp.io/cr3/dynamic-children) for more friendly and interactive documentation.

