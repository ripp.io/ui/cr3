# PathLoader

Enhance a wild-card path with the ability to 

* Call to a server for page configuration (and SEO)
* Dynamically swap out which component to load
* Provide loaded component with configuration from server call

A real-world example could be a Blog, where you wouldn't want to configure every single route up front, but instead depend on a CMS server to load the blog entry.

What you do with the data is up to you.
Could be loading HTML directly into the page, or dynamically building a page from configuration, such as configuration built from a WYSIWYG builder.

`@cr3` will be building out WYSIWYG components in the future to utilize this more. 

See [ripp.io/cr3/dynamic-children](https://ripp.io/cr3/dynamic-children) for more friendly and interactive documentation.

