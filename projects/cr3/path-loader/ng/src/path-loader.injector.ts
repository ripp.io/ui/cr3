import { Injectable, Type } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { DynamageRouteConfig } from './path-loader.types';
import { PathLoaderDefaultService } from './path-loader.service';

@Injectable()
export class DynamageRouter implements CanActivate {
  constructor(private router: Router, private service: PathLoaderDefaultService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return this.loadRoute(state.url, route);
  }

  async loadRoute(url: string, route: ActivatedRouteSnapshot): Promise<boolean> {
    try {
      const routeConfig = route.data.dynamageRoute as DynamageRouteConfig;

      // Provide full url for service logic
      routeConfig.fullUrl = url;

      // Relative url from the base path of the wild route, leaving root with '/'
      routeConfig.relativeUrl = url.replace('/' + routeConfig.basePath, '') || '/';

      const pageConfigOrPath = await this.service.handleRoute(routeConfig);

      // Path was returned - not found or redirected - navigate and block activation
      if (typeof pageConfigOrPath === 'string') {
        this.router.navigate([pageConfigOrPath]);
        return false;
      }

      const componentOrLazy = (routeConfig.componentResolver as any)(routeConfig, pageConfigOrPath);
      const component = componentOrLazy.ɵcmp ? componentOrLazy : await componentOrLazy();

      // Set data for in-flight route to be grabbed by instantiated component
      route.data = Object.assign({}, route.data, { dynamagePage: pageConfigOrPath });
      (route.routeConfig as any).component = component; // Set component to be loaded by router as normal

      return true;
    } catch (reason) {
      console.error('Dynamage: error was caught', reason);
      return false;
    }
  }

  async getComponent(routeConfig: DynamageRouteConfig, type: string): Promise<Type<any>> {
    const component: any = routeConfig.typeMap && type ? routeConfig.typeMap[type] : routeConfig.defaultComponent;

    // Component class will have ecmp object
    if (component.ɵcmp) {
      return component;
    }

    // Execute import and give back component
    return await component();
  }
}
