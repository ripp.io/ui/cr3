import { Routes } from '@angular/router';

import { DynamageRouteConfig } from './path-loader.types';
import { DynamageRouter } from './path-loader.injector';
import { mergeDefaults } from './path-loader.private-utils';

export function dynamagify(routes: Routes, appDefaults: DynamageRouteConfig = {}, nestPath = ''): Routes {
  mergeDefaults(appDefaults);
  routes.forEach(route => {
    if (route.data?.dynamageRoute) {
      if (!route.path) {
        route.path = '**';
      } else if (route.path !== '**') { // When placed on a route that's not the wildcard, create a child
        const newRoute = {path: '**', data: route.data};
        route.children = [newRoute];
        delete route.component; // No component needed
        delete route.data.dynamageRoute; // Moving data to new route

        // Correct base path
        nestPath = nestPath ? nestPath + '/' + route.path : route.path as string;
        route    = newRoute; // Replace reference for below logic
      }

      (route.data as any).dynamageRoute = Object.assign({}, appDefaults, route.data?.dynamageRoute, {basePath: nestPath});

      route.component = true as any; // It wont know the difference

      // Ensure Guard is registered
      route.canActivate = route.canActivate || [];
      if (!route.canActivate.includes(DynamageRouter)) {
        route.canActivate.push(DynamageRouter);
      }
    }

    if (route.children) {
      const newBasePath = nestPath ? nestPath + '/' + route.path : route.path;
      dynamagify(route.children, appDefaults, newBasePath);
    }
  });

  return routes;
}
