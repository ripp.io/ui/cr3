import { DynamageRouteConfig } from './path-loader.types';
import { Injector } from '@angular/core';

const routeDefaults: DynamageRouteConfig = {
  endpoint:         '/api/v1/pages',
  paramName:        'path',
  extraParams:      {},
  notFoundPath:     '/',
  errorPath:        '/',

  relativeRedirects: false,
  redirectCodes:     [301, 302, 307],

  cache:             10,
  cacheDumpInterval: 10,

  pageDefaults: {},
  componentResolver: (routeConfig, pageConfig) => (routeConfig.typeMap as any)[pageConfig.type],
  typeMap:      {}
};

export function mergeDefaults(config: DynamageRouteConfig): void {
  const withDefaults = Object.assign({}, routeDefaults, config);
  Object.assign(config, withDefaults); // Update injected config with defaults merged
}
