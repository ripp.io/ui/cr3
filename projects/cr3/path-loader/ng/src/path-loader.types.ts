import { Type } from '@angular/core';

export type ComponentOrLazyComponent = (() => Promise<Type<any>>) | Type<any>;

export interface DynamageRouteConfig {
  componentResolver?: (routeConfig: DynamageRouteConfig, pageConfig: any) => any;
  typeMap?: { [type: string]: ComponentOrLazyComponent };

  relativeRedirects?: boolean;
  defaultComponent?: ComponentOrLazyComponent;
  endpoint?: string;
  paramName?: string;
  extraParams?: { [name: string]: any };
  notFoundPath?: string;
  errorPath?: string;

  pageDefaults?: DynamagePage;
  redirectCodes?: number[];
  cache?: number;
  redirectCache?: number;
  cacheDumpInterval?: number;

  // Used at route-time only
  fullUrl?: string;
  relativeUrl?: string;
  basePath?: string;
}

export class DynamagePageCache {
  expire: number;
  config?: DynamagePage;
  redirect?: string;

  constructor(configOrRedirect: DynamagePage | string, ttlSeconds: number) {
    const expire = new Date();
    expire.setSeconds(expire.getSeconds() + ttlSeconds);
    this.expire = expire.getTime();
    if (typeof configOrRedirect === 'string') {
      this.redirect = configOrRedirect;
    } else {
      this.config = configOrRedirect;
    }
  }
}

export interface DynamagePage {
  type?: string;
  slug?: string;

  [key: string]: any;
}
