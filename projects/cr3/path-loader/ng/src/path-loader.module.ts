import { ModuleWithProviders, NgModule, Provider, Type } from '@angular/core';
import { DynamageRouter } from './path-loader.injector';
import { PathLoaderDefaultService, PathLoaderService } from './path-loader.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UniversalInterceptor } from './universal-interceptor';

@NgModule({
  declarations: [],
  imports:      [],
  exports:      [],
  providers:    [
    DynamageRouter,
    {provide: HTTP_INTERCEPTORS, useClass: UniversalInterceptor, multi: true}
  ],
})
export class PathLoaderModule {
  static forRoot(extra: any, service?: Type<PathLoaderService>, universalInterceptor = true): ModuleWithProviders<PathLoaderModule> {
    const providers: Provider[] = [];

    if (service) {
      providers.push({provide: PathLoaderDefaultService, useExisting: service});
    }

    if (universalInterceptor) {
      providers.push({provide: HTTP_INTERCEPTORS, useClass: UniversalInterceptor, multi: true});
    }

    return {
      ngModule: PathLoaderModule,
      providers
    };
  }
}
