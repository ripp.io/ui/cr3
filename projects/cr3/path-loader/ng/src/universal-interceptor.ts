import { Inject, Injectable, Optional } from '@angular/core';
import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { Observable } from 'rxjs';
import { Request } from 'express';

@Injectable()
export class UniversalInterceptor implements HttpInterceptor {
  constructor(@Optional() @Inject(REQUEST) protected request: Request) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    // If not on server or already absolute, skip
    if (!this.request || ['http', '//'].some(test => req.url.toLowerCase().startsWith(test.toLowerCase()))) {
      return next.handle(req);
    }

    const slash = req.url.startsWith('/') ? '' : '/';
    const url   = `${ this.request.protocol }://${ this.request.get('host') }${ slash }${ req.url }`;
    return next.handle(req.clone({url}));
  }
}
