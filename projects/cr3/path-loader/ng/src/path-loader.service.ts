import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { DynamagePage, DynamagePageCache, DynamageRouteConfig } from './path-loader.types';
import { catchError, map } from 'rxjs/operators';
import { makeStateKey, TransferState } from '@angular/platform-browser';

const PAGE_KEY = makeStateKey('DYNAMAGE_PAGE');

export interface PathLoaderService {
  handleRoute(routeConfig: DynamageRouteConfig): Promise<DynamagePage | string>;
}

@Injectable({providedIn: 'root'})
export class PathLoaderDefaultService implements PathLoaderService {
  private pageCache: { [key: string]: DynamagePageCache } = {};

  constructor(private http: HttpClient, private state: TransferState) {
  }

  /**
   * Return a DynamagePage for it to be loaded or a string to be redirected to
   * If redirecting/not-found, reject;
   */
  async handleRoute(routeConfig: DynamageRouteConfig): Promise<DynamagePage | string> {
    const cached = this.state.get(PAGE_KEY, this.getCachedPage(routeConfig));
    this.state.remove(PAGE_KEY); // Ensure unloaded

    if (cached) {
      return this.handleCacheRoute(routeConfig, cached);
    }

    const params = this.resolveRequestParams(routeConfig.relativeUrl as string, routeConfig);

    return this.getPage(routeConfig, params).toPromise();
  }

  getPage(routeConfig: DynamageRouteConfig, params: HttpParams): Observable<DynamagePage | string> {
    return this.http.get<DynamagePage>(routeConfig.endpoint as string, {params, observe: 'response'})
      .pipe(
        map(res => {
          if (routeConfig.redirectCodes?.includes(res.status)) {
            return this.resolveRedirect(routeConfig, res);
          }

          if (res.status < 300) {
            return this.resolvePageConfig(routeConfig, res.body as DynamagePage);
          }

          console.warn('Unknown response code ' + res.status);
          return this.resolveNotFoundPath(routeConfig);
        }),
        catchError((res: HttpResponse<any>, obs) => {
          if (res.status === 404) {
            return of(this.resolveNotFoundPath(routeConfig));
          }
          return of(this.resolveErrorPath(routeConfig));
        }),
        map(toCache => {
          if (typeof window === 'undefined') {
            // If on server, set the state and no need to cache
            this.state.set(PAGE_KEY, new DynamagePageCache(toCache, routeConfig.cache as number));
          } else {
            this.cachePage(routeConfig, toCache);
          }
          return toCache;
        }),
      );
  }

  cachePage(routeConfig: DynamageRouteConfig, pageConfigOrRedirect: DynamagePage | string): void {
    if (routeConfig.cache) {
      this.pageCache[routeConfig.fullUrl as string] = new DynamagePageCache(pageConfigOrRedirect, routeConfig.cache);

      // TODO: Test if interval exists
      // if (routeConfig.cacheDumpInterval) {
      //   setInterval(() => {
      //     const now = Date.now();
      //     Object.keys(this.pageCache).forEach(url => {
      //       if (now >= this.pageCache[url].expire) {
      //         delete this.pageCache[url];
      //       }
      //     });
      //   }, routeConfig.cacheDumpInterval * 1000);
      // }
    }
  }

  getCachedPage(routeConfig: DynamageRouteConfig): DynamagePageCache | null {
    const cached = this.pageCache[routeConfig.fullUrl as string];
    if (cached && Date.now() < cached.expire) {
      return cached;
    } else {
      delete this.pageCache[routeConfig.fullUrl as string];
      return null;
    }
  }

  handleCacheRoute(routeConfig: DynamageRouteConfig, cached: DynamagePage): DynamagePage | string {
    if (cached.redirect) {
      return cached.redirect;
    } else if (cached.config) {
      return this.resolvePageConfig(routeConfig, cached.config);
    } else {
      return this.resolveNotFoundPath(cached.routeConfig);
    }
  }

  resolveRequestParams(relativeUrl: string, routeConfig: DynamageRouteConfig): HttpParams {
    const params = new HttpParams().set(routeConfig.paramName as string, relativeUrl);
    if (routeConfig.extraParams) {
      // @ts-ignore
      Object.keys(routeConfig.extraParams).forEach(param => params.set(param, routeConfig.extraParams[param]));
    }
    return params;
  }

  resolvePageConfig(routeConfig: DynamageRouteConfig, pageConfig: DynamagePage): DynamagePage {
    return Object.assign({}, routeConfig.pageDefaults, pageConfig);
  }

  resolveRedirect(routeConfig: DynamageRouteConfig, res: HttpResponse<DynamagePage>): string {
    const target = res.headers.get('Location') as string;
    if (routeConfig.relativeRedirects) {
      return routeConfig.basePath + target;
    }
    return target;
  }

  resolveNotFoundPath(routeConfig: DynamageRouteConfig): string {
    return routeConfig.notFoundPath as string;
  }

  resolveErrorPath(routeConfig: DynamageRouteConfig): string {
    return routeConfig.errorPath as string;
  }
}
