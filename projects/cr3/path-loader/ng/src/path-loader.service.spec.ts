import { TestBed } from '@angular/core/testing';

import { PathLoaderDefaultService } from './path-loader.service';

describe('DynamageService', () => {
  let service: PathLoaderDefaultService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PathLoaderDefaultService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
