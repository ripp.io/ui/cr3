import { Express, Request, Response } from 'express';
import { APP_BASE_HREF } from '@angular/common';
import { Cache } from '@cr3/cache';

export interface RenderCacherConfig {
  cacheTtl: number;
  cacheCleanInterval?: number;
  cachePathResolver: (req: Request, config: RenderCacherConfig) => string | false;

  manualCacheClearEndpoint: string | false;
  cacheClearStatus: number;
  manualCacheDumpAuth: (req: Request) => boolean;

  sensitiveCookies: string[];
  sensitiveHeaders: string[];
  sensitiveWhitelist: string[];

  renderBypassPaths: string[];
  renderEndpoint: string;
  renderOptionsResolver: (req: Request) => object;
  sendHandler: (req: Request, res: Response, html: string, config: RenderCacherConfig) => string;
  errorHandler: (req: Request, res: Response, err: any, html: string, config: RenderCacherConfig) => void;
}

const defaults: RenderCacherConfig = {
  cacheTtl:            60,
  cacheCleanInterval:  1000,
  cachePathResolver:   defaultCachePathResolver,

  manualCacheClearEndpoint: false,
  cacheClearStatus:         204,
  manualCacheDumpAuth:      () => true,

  sensitiveCookies: [],
  sensitiveHeaders: ['X-Api-Key', 'Bearer'],
  sensitiveWhitelist: [],

  renderBypassPaths:     ['/api'],
  renderEndpoint:        '*',
  renderOptionsResolver: defaultRenderOptionsResolver,
  sendHandler:           defaultSendHandler,
  errorHandler:          defaultErrorHandler
};

export class RenderCache {
  constructor(private value: any) {
  }

  public isHtml(): boolean {
    return typeof this.value === 'string';
  }

  public getHtml(): string {
    return this.value;
  }

  public isRedirect(): boolean {
    return typeof this.value === 'object';
  }

  public getRedirect(): { status: number; location: string; } {
    return this.value;
  }
}

export function getCookies(req: Request): { [name: string]: string } {
  return req.headers.cookie?.split('; ').reduce((obj: any, cookie: string) => {
    const nameValue                 = cookie.split('=');
    obj[nameValue[0].toLowerCase()] = nameValue[1];
    return obj;
  }, {}) || {};
}

export function defaultCachePathResolver(req: Request, config: RenderCacherConfig): string | false {
  if (hasSensitiveHeader(req, config.sensitiveHeaders)) {
    return false;
  }

  if (hasSensitiveCookie(req, config.sensitiveCookies)) {
    return false;
  }

  return req.originalUrl;
}

export function hasSensitiveHeader(req: Request, sensitiveHeaders: string[]): boolean {
  if (sensitiveHeaders.length) {
    return !!sensitiveHeaders.filter(name => !!req.headers[name.toLowerCase()]).length;
  }
  return false;
}

export function hasSensitiveCookie(req: Request, sensitiveCookies: string[]): boolean {
  if (sensitiveCookies.length) {
    const cookies = getCookies(req);
    return !!sensitiveCookies.filter(name => !!cookies[name.toLowerCase()]).length;
  }
  return false;
}

export function defaultRenderOptionsResolver(req: Request): object {
  return {
    req,
    url:       `${ req.protocol }://${ (req.get('host') || '') }${ req.originalUrl }`,
    providers: [
      {provide: APP_BASE_HREF, useValue: req.baseUrl}
    ]
  };
}

export function defaultSendHandler(req: Request, res: Response, html: string, config: RenderCacherConfig): string {
  res.send(html);
  return html;
}

export function defaultErrorHandler(req: Request, res: Response, err: any, html: string): void {
  console.warn('Rendering error: ', err);
  res.send(html);
}

export class PathLoaderRenderCacher {
  private readonly config: RenderCacherConfig;
  private cache: Cache<RenderCache>;

  constructor(private server: Express, private view: string, config?: RenderCacherConfig) {
    this.config = Object.assign({}, defaults, config);
    this.cache  = new Cache<RenderCache>(this.config.cacheTtl, this.config.cacheCleanInterval);

    this.setupCacheClearEndpoint();
    this.setupRenderEndpoint();
  }

  static Handle(server: Express, view: string, config?: RenderCacherConfig | any): PathLoaderRenderCacher {
    return new PathLoaderRenderCacher(server, view, config);
  }

  private setupCacheClearEndpoint(): void {
    if (this.config.manualCacheClearEndpoint) {
      this.server.get(this.config.manualCacheClearEndpoint, ((req, res) => {
        this.cache.clear();
        res.status(this.config.cacheClearStatus);
        res.send();
      }));
    }
  }

  private setupRenderEndpoint(): void {
    this.server.get(this.config.renderEndpoint, (req, res, next) => {
      if (this.config.renderBypassPaths.some(path => req.originalUrl.toLowerCase().startsWith(path.toLowerCase()))) {
        next();
        return;
      }

      const cachePath = this.config.cachePathResolver(req, this.config);
      // const cache     = cachePath ? this.cache.get(cachePath) : null;
      const cache = null as any;

      if (cache) {
        if (cache.isRedirect()) {
          const redirect = cache.getRedirect();
          res.status(redirect.status);
          res.setHeader('Location', redirect.location);
          res.send();
        } else {
          res.send(cache.getHtml());
        }
      } else {
        res.render(this.view, this.config.renderOptionsResolver(req), (err, html) => {
          if (err) {
            this.config.errorHandler(req, res, err, html, this.config);
          } else {
            const sent = this.config.sendHandler(req, res, html, this.config);
            if (cachePath) {
              this.cache.set(cachePath, new RenderCache(sent));
            }
          }
        });
      }
    });
  }
}
