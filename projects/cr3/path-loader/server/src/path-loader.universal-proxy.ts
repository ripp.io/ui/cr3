import { Cache } from '@cr3/cache';
import { Express, NextFunction, Request, Response } from 'express';
import { IncomingMessage } from 'http';

const url = require('url');

/**
 * Resolve a request to a cache storage path.
 * Return false if no cache should be saved.
 */
export type PageCachePathResolver = (req: Request, config: PageProxyConfig) => string | false;

/**
 * Return type from get and proxy to indicate a cache should be saved
 */
export type PageCacheResponse = { key: string, pageConfig: any };

/**
 * Executed when a get is received. Meant to proxy to source of truth, respond with page's config,
 * then return a PageCacheResponse if it shoudl be cached.
 * If cached, this method is skipped and returns the cached object.
 */
export type GetHandler = (httpClient: any, serverEndpoint: string, path: string,
                          req: Request, res: Response, next: NextFunction, config: PageProxyConfig)
  => Promise<PageCacheResponse | void>;

/**
 * Create options object to be used in the proxy request.
 * Separated to allow default proxy handler but customize the request information.
 */
export type ProxyOptionsResolver = (httpClient: any, method: string, serverEndpoint: string, id: string, query: string, body: string,
                                    req: Request, res: Response, next: NextFunction, config: PageProxyConfig
) => Promise<any>;

/**
 * Proxy request to the server.
 * Resolve string of full path to have it removed from cache so a new one is grabbed on next request
 * Resolve { key: fullPath, pageConfig } if an updated cache entry is here and ready to go
 * Resolve true if cache should be completely cleared
 * Resolve void (empty) if nothing should be done
 */
export type ProxyHandler = (httpClient: any, options: any, body: string,
                            req: Request, res: Response, next: NextFunction, config: PageProxyConfig
) => Promise<string | PageCacheResponse | true | void>;

/**
 * Process response from server and formulate response to client - `done` is the resolve from ProxyHandler.
 */
export type ProxyResponseHandler = (done: any, data: string, req: Request, res: Response, next: NextFunction, config: PageProxyConfig)
  => void;

/**
 * Configuration for PageProxy - all fields are optional, if not provided will be defaulted.
 */
export interface PageProxyConfig {
  /**
   * Endpoint to listen for GET ?{paramName}= requests.
   * Proxy endpoint will be based on this with /:id added.
   */
  endpoint: string;

  /**
   * Parameter name to pull from GET request to get a full path representation of the page, such as /my/nested/page.
   * This value is used for caching.
   */
  paramName: string;

  /**
   * Function to determine path to cache the request as.
   */
  cachePathResolver: PageCachePathResolver;

  /**
   * Function to handle the GET request, returning an indicator to cache
   */
  getHandler: GetHandler;

  /**
   * Disable the proxy endpoint, leaving only the GET proxy
   */
  disableProxy: boolean;

  /**
   * Attempt to handle delete response to clear cache, looking up ${fullPathKey} on response for the cache path
   */
  handleDelete: boolean;

  /**
   * Attempt to handle put response to update cache, looking up ${fullPathKey} on response for the cache path
   */
  handlePutPatch: boolean;

  /**
   * Attempt to handle post response to add cache, looking up ${fullPathKey} on response for the cache path
   */
  handlePost: boolean;

  /**
   * Produce options for the HTTP request proxy. Be default breaks down given serverEndpoint
   */
  proxyOptionsResolver: ProxyOptionsResolver;

  /**
   * Key on proxy responses to find the full path of the page in order to cache or clear cache for it
   */
  fullPathKey: string;

  /**
   * Handle the proxy request and response - by default delegates to ${proxyResponseHandler} to handle response
   */
  proxyHandler: ProxyHandler;

  /**
   * Handle the response of the proxy, responding to the client and determining cache
   */
  proxyResponseHandler: ProxyResponseHandler;
}

/**
 * Returns the parameter value as the cache key
 */
export const defaultPageCachePathResolver: PageCachePathResolver = (req, config) => {
  return req.query[config.paramName] as string;
};

/**
 * Proxies to the ${serverEndpoint} and indicating to cache the config at the full path value
 */
export const defaultGetHandler: GetHandler = (httpClient, serverEndpoint, path, req, res, next, config) => {
  return new Promise<PageCacheResponse | void>((resolve => {
    // TODO: Query params?
    httpClient.get(serverEndpoint, (getRes: IncomingMessage) => {
      let pageConfig = '';
      getRes.setEncoding('utf8');
      getRes.on('data', (chunk: string) => pageConfig += chunk);
      getRes.on('end', () => {
        // TODO: Unmock
        pageConfig = '{"type": "custom"}';
        res.setHeader('Content-Type', 'application/json');
        res.send(pageConfig);
        resolve({key: req.query[config.paramName] as string, pageConfig});
      });
    }).on('error', (err: Error) => {
      console.log('Server page config get failed: ', err.message);
      resolve();
    });
  }));
};

/**
 * Breaks down the ${serverEndpoint} to build options for a HTTP request in the proxy handler
 */
export const defaultProxyOptionsResolver: ProxyOptionsResolver = async (httpClient, method, serverEndpoint, id, query, body) => {
  const urlParts = url.parse(serverEndpoint);
  return {
    method,
    hostname: urlParts.hostname,
    port:     urlParts.port || (urlParts.protocol === 'https:' ? 443 : 80),
    path:     `${ urlParts.path }/${ id }?${ query }`,
    headers:  {
      'Content-Type':   'application/json',
      'Content-Length': body.length
    }
  };
};

/**
 * Take the given opens, performs an HTTP Request, then delegates handling of the response to ${config.proxyResponseHandler}
 */
export const defaultProxyHandler: ProxyHandler = (httpClient, options, body, req, res, next, config) => {
  return new Promise<string | PageCacheResponse | true | void>(resolve => {
    const proxied = httpClient.request(options, (proxyRes: any) => {
      let data = '';
      proxyRes.setEncoding('utf8');
      proxyRes.on('data', (chunk: string) => data += chunk);
      proxyRes.on('end', () => config.proxyResponseHandler(resolve, data, req, res, next, config));
      proxyRes.on('error', () => {
        res.status(500);
        res.send(`Error reading proxied response at ${ options.path }${ options.query }`);
        resolve();
      });
    }).on('error', (err: Error) => {
      console.log('Server page config proxy failed: ', err.message);
      res.status(400);
      res.send();
      resolve();
    });

    proxied.write(body);
    proxied.end();
  });
};

/**
 * Responds with the proxied data, if handling put/patch/delete, attempts to get ${config.fullPathKey} and update or clear config
 */
export const defaultProxyResponseHandler: ProxyResponseHandler = (done, data, req, res, next, config) => {
  res.status(200);
  res.setHeader('Content-Type', 'application/json');
  res.send(data);

  try {
    if ((req.method === 'PUT' || req.method === 'PATCH') && config.handlePutPatch) {
      const pageConfig = JSON.parse(data);
      if (pageConfig[config.fullPathKey]) {
        done({key: pageConfig[config.fullPathKey], pageConfig});
        return;
      }
    } else if (req.method === 'DELETE' && config.handleDelete) {
      const pageConfig = JSON.parse(data);
      if (pageConfig[config.fullPathKey]) {
        done(pageConfig[config.fullPathKey]);
        return;
      }
    }
  } catch (err: any) {
    console.error(`Page proxy ${ req.url } response was not valid JSON`, err);
  }
  done();
};

const defaults: PageProxyConfig = {
  endpoint:          '/api/v1/pages',
  paramName:         'path',
  cachePathResolver: defaultPageCachePathResolver,
  getHandler:        defaultGetHandler,

  disableProxy: false,
  handleDelete:      false,
  handlePutPatch:    true,
  handlePost:        true,
  proxyOptionsResolver: defaultProxyOptionsResolver,
  fullPathKey:       'fullPath',
  proxyHandler:         defaultProxyHandler,
  proxyResponseHandler: defaultProxyResponseHandler
};

/**
 * Page Proxy establishes a GET and optionally POST/PUT/PATCH/DELETE with /:id proxies to be able to cache dynamic page
 * data flowing between client and server, creating a cache closer to the client, relieving work on both sides.
 * In use with Angular Universal, allows server-side rendering to grab from a local cache, speeding up rendering.
 * Use along side RenderCacher to create a powerful cache system for dynamic paging.
 */
export class PathLoaderUniversalProxy {
  private readonly config: PageProxyConfig;
  private cache: Cache<string>;

  constructor(private server: Express, private serverEndpoint: string, private httpClient: any, config?: PageProxyConfig) {
    this.config = Object.assign({}, defaults, config);
    this.cache  = new Cache<string>();

    this.setupPageGetEndpoint();
    if (!this.config.disableProxy) {
      this.setupPageProxyEndpoint();
    }
  }

  /**
   * Static helper to establish a Page Proxy
   * @param server Express server instance to add routes to
   * @param serverEndpoint Server endpoint to proxy requests to
   * @param httpClient http or https depending server hosting
   * @param config Any configuration overrides, such as swapping out a handler
   */
  static Handle(server: Express, serverEndpoint: string, httpClient: any, config?: PageProxyConfig | any): PathLoaderUniversalProxy {
    return new PathLoaderUniversalProxy(server, serverEndpoint, httpClient, config);
  }

  private setupPageGetEndpoint(): void {
    this.server.get(this.config.endpoint, async (req, res, next) => {
      const path   = this.config.cachePathResolver(req, this.config);
      const cached = path ? this.cache.get(path) : null;

      if (cached) {
        res.send(cached);
        return;
      }

      const toCache = await this.config.getHandler(this.httpClient, this.serverEndpoint, path as string, req, res, next, this.config);
      if (toCache) {
        this.cache.set(toCache.key, toCache.pageConfig);
      }
    });
  }

  private setupPageProxyEndpoint(): void {
    this.server.all(this.config.endpoint + '/:id', (req, res, next) => {
      let body = '';
      req.on('data', chunk => body += chunk);
      req.on('end', async () => {
        const options = await this.config.proxyOptionsResolver(
          this.httpClient,
          req.method,
          this.serverEndpoint,
          req.params.id,
          url.parse(req.url).query,
          body,
          req, res, next, this.config
        );

        if (!options) {
          res.status(500);
          res.send('Options for proxy were not determined');
        }

        const fullPathKey = await this.config.proxyHandler(
          this.httpClient, options, body,
          req, res, next, this.config
        );

        if (fullPathKey) {
          if (fullPathKey === true) {
            this.cache.clear();
          } else if (typeof fullPathKey === 'string') {
            // Remove cache as a new one is needed from the server
            this.cache.remove(fullPathKey);
          } else {
            // Update cached page config if given
            this.cache.set(fullPathKey.key, fullPathKey.pageConfig);
          }
        }
      });

    });
  }
}
