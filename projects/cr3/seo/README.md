# Search Engine Optimization

See [ripp.io/cr3/seo](https://ripp.io/cr3/seo) for more friendly and interactive documentation.

Dynamically updating the DOM to provide `<title>`, `<meta>`, `<link>` or other tags for a meaningful SEO experience.

> Note this is mostly (only) useful when using a Server-Side Rendering (SSR) solution such as Angular Universal, so scrapers/bots see the rendered page with the tags already there.

This project is broken into multiple solutions for reusability. Currently:
* [Core](#core) (`@cr3/seo`)
  * Heart of the solution to be used directly or wrapped in framework solutions
```typescript
@import { SeoManager } from '@cr3/seo';

const seoManager = new SeoManager();
seoManager.update(...);
```
* [Angular](#angular) (`@cr3/seo/ng`)
  * Wrapper to provide [SeoService](#angular) optionally (by default) with a Router Event Listener
```typescript
@import { SeoModule } from '@cr3/seo/ng';

...
@NgModule({
  ...
  imports: [
    ...
    SeoModule,
  ],
```
  
## Core

```typescript
import { SeoMeta, SeoConfig, defaultMetaMapping } from '@cr3/seo';

const seoConfig: SeoConfig = {
  defaults:            {} as SeoMeta, // (1) Set Default SEO options
  mappings:            defaultMetaMapping, // (2) See Mapping section
  fallbackCanonical:   true, // Grab current URL for canonical on every update

  // Everything below will attempt to grab current value at 
  //   instantiation and use as a default for every update
  fallbackTitle:       true,
  fallbackDescription: true,
  fallbackIcon:        true,
  fallbackRobots:      true,
  fallbackKeywords:    true,
  fallbackAuthor:      true,
  fallbackCopyright:   true
};
const seoManager = new SeoManager(document, seoConfig); // (3)

// .....

const seoUpdate: SeoMeta = { // (4)
  title: 'Updated Title'
}
seoManager.update(seoUpdate); // (5)
```

1) Default SEO settings will be used as a fallback when `seoManager.update` has no setting for the field
2) See [Mappings](#mappings)
   * Mappings provided here will replace all mappings - will use `defaultMetaMapping` if not supplied here
   * `defaultMetaMapping` is available to copy from or programmatically filter/add to
3) Create an instance of the manager
   * `document` is needed as this is designed to run server-side where no document is available. In Angular Universal `@Inject(DOCUMENT) private readonly document: Document` is used to get reference to the document
   * A third parameter, `SeoMeta` (not shown above), can be passed in to set the current SEO
     * This can be used if you pass state from the server-rendering to the browser as prevent a little extra processing
4) Example of setting metadata - likely come from data tied to the page, or a mapping of pages to SEO configurations.
5) Call `update(SeoMeta)` to reset SEO tags to defaults mixed with the `SeoMeta` sent in
   * Internally it will compare the current SEO with this new one to determine actions to take, preventing excess DOM interaction
   * Will also utilize existing DOM elements when possible

> Note that `SeoMeta` has predefined fields but also lets you add whatever field you wish, which can then be [mapped](#mappings) to.

### Mappings

This refers to mapping HTML DOM elements to fields of the `SeoMeta` object.

A few examples, from `defaultMetaMapping`:
```typescript
const exampleMapping: SeoMetaMapping = {
  // Page Title
  title: {
    tag:       'title',   // Set element tag to '<title>'
    innerText: '${title}' // Set innerText to `SeoMeta.title`
  },

  // Page Description
  description: {
    tag:   'meta', // Set element tag to '<meta>'
    attrs: {
      name:    '!', // `name` attribute should be 'description'
                    // '!' is used to tell the processor to use the key of this entry
                    // Alternatively, you could put 'description' as the value here
      content: '${description}' // Set 'content' attribute to `SeoMeta.description`
    }
  },
  
  // Canonical Link
  canonical:   {
    tag:   'link', // `<link>` element
    attrs: {
      rel:  '!', // `rel="canonical"`
      href: '${canonical}' // `SeoMeta.canonical`
    }
  },

  // Favorite Icon
  icon: {
    tag:   'link', // `<link>` element
    attrs: {
      rel:  '!',            // `rel="icon"`
      href: '${icon}',      // `SeoMeta.icon`
      type: 'image/x-icon'  // `type="image/x-icon"`
                            // Some meta tags require more attributes like this
                            //  They're also used in DOM element reuse - when a SEO
                            //    update is performed, it checks if there's already
                            //    an element, if not creates it and append to `<head>`
    }
  },
  
  // Custom Example
  custom: {
    tag: 'meta', // `<meta>` element
    attrs: {
      content: '${customField}',      // `SeoMeta.customField`
      nested: '${nested.customField}' // `SeoMeta.nested.customField`
    }
  }
}
```

`SeoMeta` allows for custom fields to be added, allowing for custom elements and mappings.

This utilizes [`stringTemplate` from `@cr3/utils`](http://ripp.io/cr3/utils), essentially treating the provided strings as [template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals) with the context of the current `SeoMeta`

When any of the mentioned `SeoMeta` fields are updated, the corresponding meta element is updated/created/removed.

### Functional Concept

The internal workings are relitively simple.

Each `attr` and `innerText` of the mappings are regex scraped to build up a map keyed by `SeoMeta` fields that drives the element.

When `update(SeoMeta)` is called, the differences between new and current `SeoMeta` object are identified, then using this mapping, the HTML DOM elements are updated/created/removed as needed.


## Angular

An Angular `SeoService` is provided at `@cr3/seo/ng`.
This can be used directly or simply import the `SeoModule`.

The module will perform the `initialize()` on the service, optionally supplying it with config.

```typescript
imports: [
  ...
  SeoModule
],

// Or supply configuration
imports: [
  ...
  SeoModule.withConfig({} as SeoServiceConfig)
],
```

This config has a couple extra options from [core](#core): 

```typescript
manualMap?: MetaMap;
listenToRoute?: boolean;
```

`listenToRoute` tells the service to establish the router event subscription when `initialize` is called

`manualMap` is a map of URL to an SeoMeta config, allowing you to centralize your SEO configurations. Note that there are 2 other ways to supply SEO configuration.

The route event will look in 3 places for SEO configuration, in this order:
* `manualMap` described above
* The activated route's `data.seo` object
* The component to be activated can have a `static SEO` field

> The [`@cr3/page-loader`](https://ripp.io/cr3/page-loader) dynamically sets `data.seo` with data from the server on page routing

The found SEO config will be used to then call the `SeoService.setSeo(...)`, which proxies to `SeoManager.update(...)`.

`SeoService` also has logic to utilize `TransferState` when ran in a Angular Universal environment.
If `BrowserTransferStateModule` is imported to your `AppModule`, it will transfer the current SEO state from server to browser and prevent a little excess processing.

