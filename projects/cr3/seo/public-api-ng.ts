export { SeoServiceConfig, SeoService } from './ng/src/seo.service';
export { SeoModule } from './ng/src/seo.module';
export * from './ts/public-api';
