import { SeoConfig, SeoMeta } from './seo.types';
import { defaultMetaMapping, SeoMetaMapping, SeoMetaTag } from './seo.mapping';
import { stringTemplate } from '@cr3/utils';

export const SEO_DEFAULTS: SeoConfig = {
  mappings:            defaultMetaMapping,
  fallbackCanonical:   true,
  fallbackTitle:       true,
  fallbackDescription: true,
  fallbackIcon:        true,
  fallbackRobots:      true,
  fallbackKeywords:    true,
  fallbackAuthor:      true,
  fallbackCopyright:   true
};

export const SEO_META_DEFAULTS: SeoMeta = {
  robots:   'index, follow',
  viewport: 'width=device-width,initial-scale=1.0'
};

interface MetaRef extends SeoMetaTag {
  el?: HTMLElement;
}

export class SeoManager {
  private metaToTags: { [metaName: string]: MetaRef }      = {};
  private configToTags: { [configKey: string]: MetaRef[] } = {};
  private headEl: HTMLHeadElement;
  private readonly defaults: SeoMeta;
  private fallbackCanonical?: boolean;
  private currentSeo: SeoMeta;

  constructor(private document: Document, config?: SeoConfig, currentSeo: SeoMeta = {}) {
    config      = Object.assign({}, SEO_DEFAULTS, config);
    this.fallbackCanonical = config?.fallbackCanonical;
    this.headEl = document.head;
    this.parseMappings(config.mappings || {});
    this.defaults   = this.processDefaults(config);
    this.currentSeo = currentSeo;
  }

  private processDefaults(config: SeoConfig): SeoMeta {
    const defaults = Object.assign({}, SEO_META_DEFAULTS, config.defaults);

    // Save off current value in DOM as default -- is there a better way?
    if (config.fallbackTitle && this.configToTags.title) {
      defaults.title = this.configToTags.title.find(ref => ref.el?.innerText)?.el?.innerText;
    }
    if (config.fallbackDescription && this.configToTags.description) {
      defaults.description = this.configToTags.description
        .find(ref => ref.el?.getAttribute('content'))?.el?.getAttribute('content') as string;
    }
    if (config.fallbackIcon && this.configToTags.icon) {
      defaults.icon = this.configToTags.icon
        .find(ref => ref.el?.getAttribute('href'))?.el?.getAttribute('href') as string;
    }
    if (config.fallbackRobots && this.configToTags.robots) {
      defaults.robots = this.configToTags.robots
        .find(ref => ref.el?.getAttribute('content'))?.el?.getAttribute('content') as string || defaults.robots;
    }
    if (config.fallbackKeywords && this.configToTags.keywords) {
      const str = this.configToTags.keywords
        .find(ref => ref.el?.getAttribute('content'))?.el?.getAttribute('content') as string;
      if (str) {
        defaults.keywords = str.split(', ');
      }
    }
    if (config.fallbackAuthor && this.configToTags.author) {
      defaults.author = this.configToTags.author
        .find(ref => ref.el?.getAttribute('content'))?.el?.getAttribute('content') as string;
    }
    if (config.fallbackCopyright && this.configToTags.copyright) {
      defaults.copyright = this.configToTags.copyright
        .find(ref => ref.el?.getAttribute('content'))?.el?.getAttribute('content') as string;
    }

    // Cheap cleanup for compare
    Object.keys(defaults).forEach(configKey => {
      if (defaults[configKey] === undefined) {
        delete defaults[configKey];
      }
    });
    return defaults;
  }

  private parseMappings(mappings: SeoMetaMapping): void {
    const regex = /\${(.*?)(?:.join\(.*?\))*?}/g;

    for (const [seoKey, metaConfig] of Object.entries(mappings)) {
      const metaRef: MetaRef = this.metaToTags[seoKey] = Object.assign({}, metaConfig);

      // Gather all possible areas a templated field from config may be
      const toTest = [];
      if (metaRef.attrs) {
        Object.entries(metaRef.attrs).reduce((arr: string[], [attr, value]) => {
          if (value === '!') {
            // Set easy pointer to seoKey
            (metaRef.attrs as any)[attr] = seoKey;
          } else {
            toTest.push(value);
          }
          return arr;
        }, []);
      }
      if (metaRef.innerText) {
        toTest.push(metaRef.innerText);
      }

      // Test all possible areas and figure out what config fields drive this meta tag
      const drivers = toTest.reduce((arr: string[], value) => {
        let m = regex.exec(value);

        if (m !== null) {
          while (m !== null) {
            if (m.index === regex.lastIndex) {
              console.error('empty match');
              regex.lastIndex++;
            }

            arr.push(m[1]);
            m = regex.exec(value);
          }
        }
        return arr;
      }, []);

      if (drivers.length) {
        // Attempt to find, for things like default title fallback
        metaRef.el = this.findElement(metaRef);

        // Create a map from config key to elements they drive - facebookAppId to fb:app_id element ref
        drivers.forEach(configKey => {
          this.configToTags[configKey] = this.configToTags[configKey] || [];
          this.configToTags[configKey].push(metaRef);
        });
      } else {
        // If there's no drivers, the meta element is static - ensure it's in the DOM and forget about it
        this.updateElement(metaRef, false);
      }
    }

  }

  private resolveElement(ref: MetaRef): HTMLElement {
    if (ref.el) {
      return ref.el;
    }

    const fromDom = this.findElement(ref);
    if (fromDom) {
      return fromDom;
    }

    return ref.el = this.document.createElement(ref.tag);
  }

  private findElement(ref: MetaRef): HTMLElement | undefined {
    const selectors = [ref.tag];

    // Add attribute that don't have templated strings to be able to select specific elements from the DOM
    if (ref.attrs) {
      for (const [attr, value] of Object.entries(ref.attrs)) {
        if (!value.includes('${')) {
          selectors.push(`[${ attr }="${ value }"]`);
        }
      }
    }

    return this.headEl.querySelector(selectors.join('')) as any;
  }

  private updateElement(ref: MetaRef, processTemplates = true): void {
    // Evaluate once instead of for each attribute
    const valueProcessor = processTemplates
                           ? (value: string) => stringTemplate(value, this.currentSeo)
                           : (value: string) => value;

    ref.el = this.resolveElement(ref);

    if (ref.attrs) {
      Object.entries(ref.attrs).forEach(([attr, value]) => ref.el?.setAttribute(attr, valueProcessor(value)));
    }
    if (ref.innerText) {
      ref.el.innerText = valueProcessor(ref.innerText);
    }

    if (!ref.el.isConnected) {
      // Throw element in <head> if not already in DOM
      this.headEl.appendChild(ref.el);
    }
  }

  public update(seo?: SeoMeta): SeoMeta {
    const oldSeo    = this.currentSeo;
    this.currentSeo = Object.assign({}, this.defaults, seo);
    if (this.fallbackCanonical && !this.currentSeo.canonical) {
      this.currentSeo.canonical = this.document.location.href;
    }

    for (const [configKey, value] of Object.entries(oldSeo)) {
      if (this.currentSeo[configKey]) {
        if (value !== this.currentSeo[configKey]) {
          this.configToTags[configKey].forEach(ref => this.updateElement(ref));
        }
      } else {
        this.configToTags[configKey].forEach(ref => ref.el?.remove());
      }
    }

    Object.keys(this.currentSeo).forEach(configKey => {
      if (!oldSeo[configKey]) {
        this.configToTags[configKey].forEach(ref => this.updateElement(ref));
      }
    });

    return this.currentSeo;
  }

}
