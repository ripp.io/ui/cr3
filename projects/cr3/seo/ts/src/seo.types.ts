import { ActivationEnd, NavigationEnd } from '@angular/router';
import { SeoMetaMapping, SeoMetaTag } from './seo.mapping';

// https://developers.facebook.com/tools/debug/
// https://cards-dev.twitter.com/validator
// https://www.linkedin.com/post-inspector/inspect/
// https://search.google.com/test/rich-results

export interface SeoMeta {
  ////////////////////////////////////////////////////////////
  // Global [likely]
  ////////////////////////////////////////////////////////////
  siteName?: string;
  icon?: string;
  logo?: string;
  robots?: string;
  author?: string;
  copyright?: string;
  viewport?: string; // https://developer.mozilla.org/en-US/docs/Mozilla/Mobile/Viewport_meta_tag
  locale?: string;
  localeAlt?: string;
  languageAlternateHref?: string; // See https://moz.com/learn/seo/hreflang-tag
  languageAlternate?: string; // See https://moz.com/learn/seo/hreflang-tag

  ////////////////////////////////////////////////////////////
  // Page Info
  ////////////////////////////////////////////////////////////
  title?: string; // Recommended 60 characters max
  description?: string; // Recommended 50 - 160 characters
  canonical?: string;
  type?: string; // https://ogp.me/#types
  keywords?: string[];

  ////////////////////////////////////////////////////////////
  // Media
  ////////////////////////////////////////////////////////////
  image?: string;
  imageAlt?: string;
  audio?: string;
  audioAlt?: string;
  video?: string;
  videoAlt?: string;
  player?: string;
  stream?: string;

  ////////////////////////////////////////////////////////////
  // Social Specifics
  ////////////////////////////////////////////////////////////
  facebookAppId?: string; // 123456789
  facebookAdmins?: string[]; // ['userid1', 'userid2']
  twitterCreator?: string; // @exampleuser
  twitterSite?: string; // @examplesite

  ////////////////////////////////////////////////////////////
  // Apps
  ////////////////////////////////////////////////////////////
  iphoneAppName?: string;
  iphoneAppId?: string;
  iphoneAppUrl?: string;

  // Defaults to iphone settings
  ipadAppName?: string;
  ipadAppId?: string;
  ipadAppUrl?: string;

  googleplayAppId?: string;
  googleplayAppName?: string;
  googleplayAppUrl?: string;

  ////////////////////////////////////////////////////////////
  // Content Publishing - likely articles
  ////////////////////////////////////////////////////////////
  contentAuthor?: string;
  contentPublisher?: string;
  contentSection?: string;
  contentTags?: string[];
  contentPublishedAt?: string;
  contentModifiedAt?: string;
  contentExpiresAt?: string;

  ////////////////////////////////////////////////////////////
  // Whatever your heart desires, but please notify if missing something useful
  ////////////////////////////////////////////////////////////
  [key: string]: any;
}
export type MetaMap = { [url: string]: SeoMeta };

export class SeoConfig {
  defaults?: SeoMeta;
  fallbackCanonical?: boolean;
  fallbackTitle?: boolean;
  fallbackDescription?: boolean;
  fallbackIcon?: boolean;
  fallbackRobots?: boolean;
  fallbackKeywords?: boolean;
  fallbackAuthor?: boolean;
  fallbackCopyright?: boolean;

  manualMap?: MetaMap;
  mappings?: SeoMetaMapping;
}
