import { Inject, Injectable, Optional } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { filter, map } from 'rxjs/operators';
import { MetaMap, SeoConfig, SeoManager, SeoMeta } from '../../ts/public-api';
import { makeStateKey, TransferState } from '@angular/platform-browser';

const SEO_KEY = makeStateKey('CR3_SEO');

export class SeoServiceConfig extends SeoConfig {
  manualMap?: MetaMap;
  listenToRoute?: boolean;
}

const DEFAULTS: SeoServiceConfig = {
  listenToRoute: true,
};

/** @dynamic */
@Injectable({ providedIn: 'root' })
export class SeoService {
  protected seoManager?: SeoManager;
  private manualMap: MetaMap = {};
  private routeSub?: Subscription;
  private meta?: SeoMeta;

  constructor(
    private readonly router: Router,
    @Inject(DOCUMENT) private readonly document: Document,
    private readonly activatedRoute: ActivatedRoute,
    @Optional() private state: TransferState,
  ) {}

  initialize(config?: SeoServiceConfig): SeoService {
    config = Object.assign({}, DEFAULTS, config);

    if (this.state && typeof window !== 'undefined') {
      // Running in browser, see if there's saved meta
      this.meta = this.state.get(SEO_KEY, {});
      this.state.remove(SEO_KEY);
    }

    this.seoManager = new SeoManager(this.document, config, this.meta);
    this.manualMap   = config?.manualMap || {};

    if (config?.listenToRoute) {
      this.registerRouter();
    }
    return this;
  }

  registerRouter(): SeoService {
    if (this.routeSub) {
      throw new Error('SEO Route listener already subscribed');
    }

    this.routeSub = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map(route => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }),
      filter(route => route.outlet === 'primary')
    ).subscribe((route: ActivatedRoute) => {
      // Resolve route to meta config
      // @ts-ignore
      const url = this.activatedRoute.snapshot._routerState.url;
      const meta = this.manualMap[url] || route.snapshot.data.seo || (route.component as any)?.SEO;

      this.setSeo(meta);
    });
    return this;
  }

  setSeo(meta?: SeoMeta): SeoService {
    if (!this.seoManager) {
      throw new Error('SEO service has not been initialized');
    }

    this.meta = this.seoManager.update(meta);
    if (this.state && typeof window === 'undefined') {
      // Running in universal, save off SEO to pre-load client
      this.state.set(SEO_KEY, this.meta);
    }
    return this;
  }

  getCurrentMeta(): SeoMeta {
    return Object.assign({}, this.meta);
  }
}
