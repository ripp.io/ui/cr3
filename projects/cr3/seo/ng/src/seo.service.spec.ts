import { fakeAsync, TestBed, tick } from '@angular/core/testing';

import { SeoService } from './seo.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { SeoMeta } from '@cr3/seo';

const dataExample = { title: 'Data Example' };
const staticExample = { title: 'Static Example' };
const manualExample = { title: 'Manual Example' };

@Component({
  selector: 'cr3-static',
  template: ''
})
class SeoStaticComponent {
  static SEO = staticExample;
}
@Component({
  selector: 'cr3-data',
  template: ''
})
class SeoDataComponent {}
@Component({
  selector: 'cr3-manual',
  template: ''
})
class SeoManualComponent {}

describe('SeoService', () => {
  let service: SeoService;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        SeoStaticComponent,
        SeoDataComponent,
        SeoManualComponent,
      ],
      imports: [
        RouterTestingModule.withRoutes([
          { path: '', component: SeoManualComponent }, // Reusing manual for blank -- nothing tied to it
          { path: 'static', component: SeoStaticComponent },
          { path: 'data', component: SeoDataComponent, data: { seo: dataExample } },
          { path: 'manual', component: SeoManualComponent },
        ])
      ]
    });
    service = TestBed.inject(SeoService);
    router = TestBed.inject(Router);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    // @ts-ignore
    expect(service.seoManager).toBeFalsy();
    // @ts-ignore
    expect(service.routerSub).toBeFalsy();

  });

  describe('initialization', () => {
    it('should initialize with defaults', fakeAsync(() => {
      service.initialize({ manualMap: { '/manual': manualExample }});
      // @ts-ignore
      expect(service.seoManager).toBeTruthy();
      // @ts-ignore
      const routeSub = service.routeSub;
      expect(routeSub).toBeTruthy();

      // Should shortcut and not double register
      expect(() => service.registerRouter()).toThrowError();

      const setSeoSpy        = spyOn(service, 'setSeo');

      // Root has no SEO settings
      router.navigate(['/']);
      tick();
      expect(setSeoSpy).toHaveBeenCalledTimes(1);
      expect(setSeoSpy).toHaveBeenCalledWith(undefined); // No manual, data, or static SEO found

      // Get from Static SEO Field on component
      router.navigate(['/static']);
      tick();
      expect(setSeoSpy).toHaveBeenCalledTimes(2);
      expect(setSeoSpy).toHaveBeenCalledWith(staticExample);

      // Get from data on route config
      router.navigate(['/data']);
      tick();
      expect(setSeoSpy).toHaveBeenCalledTimes(3);
      expect(setSeoSpy).toHaveBeenCalledWith(dataExample);

      // Get from matching path in manual config
      router.navigate(['/manual']);
      tick();
      expect(setSeoSpy).toHaveBeenCalledTimes(4);
      expect(setSeoSpy).toHaveBeenCalledWith(manualExample);

      routeSub?.unsubscribe(); // Cleanup
    }));

    it('should not listen to routes', fakeAsync(() => {
      service.initialize({ listenToRoute: false });
      // @ts-ignore
      expect(service.routeSub).toBeFalsy();

      const setSeoSpy        = spyOn(service, 'setSeo');
      router.navigate(['/']);
      tick();
      expect(setSeoSpy).toHaveBeenCalledTimes(0);
    }));

    it('should send onto manager', fakeAsync(() => {
      expect(() => service.setSeo()).toThrowError();

      service.initialize({ listenToRoute: false });
      // @ts-ignore
      const manager: SeoManager = service.seoManager;
      const updateSpy = spyOn(manager, 'update');

      const meta = { test: 'stuff' };
      service.setSeo(meta);
      expect(updateSpy).toHaveBeenCalledTimes(1);
      expect(updateSpy).toHaveBeenCalledWith(meta);
    }));
  });
});
