import { ModuleWithProviders, NgModule, Optional } from '@angular/core';
import { SeoService, SeoServiceConfig } from './seo.service';

@NgModule({
  declarations: [],
  imports:      [],
  exports:      []
})
export class SeoModule {
  constructor(seoService: SeoService, @Optional() config?: SeoServiceConfig) {
    seoService.initialize(config);
  }

  static withConfig(config: SeoServiceConfig): ModuleWithProviders<SeoModule> {
    return {
      ngModule:  SeoModule,
      providers: [{ provide: SeoServiceConfig, useValue: config }],
    };
  }
}
