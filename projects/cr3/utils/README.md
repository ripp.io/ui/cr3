# Utils

Basic Typescript/Javascript and framework utilities.

See [ripp.io/cr3/utils](https://ripp.io/cr3/utils) for more friendly and interactive documentation.

* [Strings](./ts/src/utils.strings.ts)
