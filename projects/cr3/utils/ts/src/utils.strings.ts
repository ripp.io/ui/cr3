export function stringTemplate(str: string, vars: any, defaults: any = {}, fallback = ''): string {
  const ctx = Object.assign({_fallback: fallback}, defaults, vars);
  return new Function('return `' + str.replace(/\${(.*?)}/g, '${this.$1 || this._fallback}') + '`;').call(ctx);
}
