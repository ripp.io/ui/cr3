import { stringTemplate } from './utils.strings';

describe('strings', () => {
  describe('template', () => {
    const vars  = {
      var1: 'ex1',
      var2: 'ex2'
    };
    const tests = {
      simple:           ['${var1}', vars.var1],
      simpleExt:        ['${var1}/test', `${ vars.var1 }/test`],
      simpleTwo:        ['${var1}${var2}', `${ vars.var1 }${ vars.var2 }`],
      simpleTwoExt:     ['${var1}/${var2}', `${ vars.var1 }/${ vars.var2 }`],
      nothingToDo:      ['test', 'test'],
      unknownNoDefault: ['${var3}', '']
    };

    Object.entries(tests).forEach(([test, [input, expected]]) => {
      it(test, () => expect(stringTemplate(input, vars)).toEqual(expected));
    });

    const defaults = { var3: 'ex3' };
    const flip     = '(╯°□°)╯︵ ┻━┻';

    it('use default value', () => expect(stringTemplate('${var3}', vars, defaults)).toEqual(defaults.var3));
    it('use provided fallback value', () => expect(stringTemplate('${var3}', vars, {}, flip)).toEqual(flip));
    it('use default when fallback is provided', () => expect(stringTemplate('${var3}', vars, defaults, flip)).toEqual(defaults.var3));
    it('use provided fallback if no default', () => expect(stringTemplate('${var4}', vars, defaults, flip)).toEqual(flip));
  });

});
