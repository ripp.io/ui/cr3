import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoUtilsRoutingModule } from './demo-utils-routing.module';
import { DemoUtilsComponent } from './demo-utils.component';


@NgModule({
  declarations: [DemoUtilsComponent],
  imports: [
    CommonModule,
    DemoUtilsRoutingModule
  ]
})
export class DemoUtilsModule { }
