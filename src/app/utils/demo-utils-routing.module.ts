import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoUtilsComponent } from './demo-utils.component';

const routes: Routes = [{ path: '', component: DemoUtilsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoUtilsRoutingModule { }
