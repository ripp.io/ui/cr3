import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoDynamicChildrenRoutingModule } from './demo-dynamic-children-routing.module';
import { DemoDynamicChildrenComponent } from './demo-dynamic-children.component';
import { DynamicChildrenModule } from '@cr3/dynamic-children';


@NgModule({
  declarations: [DemoDynamicChildrenComponent],
  imports: [
    CommonModule,
    DemoDynamicChildrenRoutingModule,
    DynamicChildrenModule
  ]
})
export class DemoDynamicChildrenModule { }
