import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoDynamicChildrenComponent } from './demo-dynamic-children.component';

const routes: Routes = [{ path: '', component: DemoDynamicChildrenComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoDynamicChildrenRoutingModule { }
