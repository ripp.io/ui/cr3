import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'demo-demo-dynamic-children',
  templateUrl: './demo-dynamic-children.component.html',
  styleUrls: ['./demo-dynamic-children.component.scss']
})
export class DemoDynamicChildrenComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
