import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoDynamicChildrenComponent } from './demo-dynamic-children.component';

describe('DemoDynamicChildrenComponent', () => {
  let component: DemoDynamicChildrenComponent;
  let fixture: ComponentFixture<DemoDynamicChildrenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoDynamicChildrenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoDynamicChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
