import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DemoSeoComponent } from './demo-seo.component';
import { SeoMeta } from '@cr3/seo/ng';
import { DemoSeoDataComponent, DemoSeoManualComponent, DemoSeoStaticComponent } from './demo-seo-example.component';

const routes: Routes = [
  {
    path: '', component: DemoSeoComponent, children: [
      {
        path:      'manual',
        component: DemoSeoManualComponent,
      },
      {
        path:      'data',
        component: DemoSeoDataComponent,
        data:         {
          seo: {
                 title:       'Data-Driven SEO',
                 description: 'Example of SEO maintained on the data of a route, the most preferred route... kinda...'
               } as SeoMeta
        }
      },
      {
        path:      'static',
        component: DemoSeoStaticComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoSeoRoutingModule {}
