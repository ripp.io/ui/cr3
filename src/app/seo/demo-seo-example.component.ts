import { Component } from '@angular/core';
import { SeoMeta } from '@cr3/seo/ng';

const manualTemplate = `<h1>Manual Map-Driven SEO</h1>
<div>See <a href="https://gitlab.com/ripp.io/ui/cr3/-/blob/master/src/app/seo/demo-manual-map.ts" target="_blank">manual map config</a> and <a href="https://gitlab.com/ripp.io/ui/cr3/-/blob/master/src/app/app.module.ts#L17" target="_blank">module import</a> for manual map example.</div>
<pre class="code"><code [innerHTML]="code"></code></pre>
`;

const dataTemplate = `<h1>Route Data-Driven SEO</h1>
<div>See <a href="https://gitlab.com/ripp.io/ui/cr3/-/blob/master/src/app/seo/demo-seo-routing.module.ts#L18" target="_blank">router config</a> for data example.</div>
<pre class="code"><code [innerHTML]="code"></code></pre>
`;

const staticTemplate = `<h1>Static SEO Field on Component-Driven SEO</h1>
<div>See <a href="https://gitlab.com/ripp.io/ui/cr3/-/blob/master/src/app/seo/demo-seo-example.component.ts#L58" target="_blank">this component's static SEO field</a> for static example.</div>
<pre class="code"><code [innerHTML]="code"></code></pre>
`;

@Component({
  selector: 'demo-seo-manual',
  template: manualTemplate,
  styles: []
})
export class DemoSeoManualComponent {
        public code = `import { MetaMap, SeoModule } from '@cr3/seo/ng';

export const manualMap: MetaMap = {
    title:       'Manual SEO Mapping',
    description: 'Example of SEO maintained as a manual map to centralize all SEO config'
  }
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports:      [
    BrowserModule,
    AppRoutingModule,
    SeoModule.withConfig({ manualMap }) // <----
  ],
  providers:    [],
  bootstrap:    [AppComponent]
})
export class AppModule {}`;
}

@Component({
  selector: 'demo-seo-data',
  template: dataTemplate,
  styles: []
})
export class DemoSeoDataComponent {
  public code = `{
  path: 'data',
  component: DemoSeoDataComponent,
  data: {
    seo: {
           title:       'Data-Driven SEO',
           description: 'Example of SEO maintained on the data of a route, the most preferred route... kinda...'
         } as SeoMeta
  }
},`;
}

@Component({
  selector: 'demo-seo-static',
  template: staticTemplate,
  styles: []
})
export class DemoSeoStaticComponent {
  static SEO: SeoMeta = {
    title: 'Statically Set SEO',
    description: 'Example of SEO maintained on the component as a static variable'
  };

  public code = `@Component({
  selector: 'demo-seo-static',
  template: staticTemplate,
  styles: []
})
export class DemoSeoStaticComponent {
  static SEO: SeoMeta = {
    title: 'Statically Set SEO',
    description: 'Example of SEO maintained on the component as a static variable'
  };
}`;
}
