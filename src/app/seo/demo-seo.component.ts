import { Component, OnInit } from '@angular/core';
import { SeoMeta, SeoService } from '@cr3/seo/ng';

@Component({
  selector:    'demo-demo-seo',
  templateUrl: './demo-seo.component.html',
  styleUrls:   ['./demo-seo.component.scss']
})
export class DemoSeoComponent implements OnInit {
  constructor(public seoService: SeoService) {}

  public code = `@NgModule({
  declarations: [
    AppComponent
  ],
  imports:      [
    BrowserModule,
    AppRoutingModule,
    SeoModule        // <----
  ],
  providers:    [],
  bootstrap:    [AppComponent]
})
export class AppModule {}`;

  public mappings = '';
  public elements = '';

  ngOnInit(): void {
    // @ts-ignore
    const metaToTags = this.seoService.seoManager.metaToTags;
    // @ts-ignore
    const configToTags = this.seoService.seoManager.configToTags;

    this.mappings = Object.entries(configToTags).reduce((str, [field, tags]) => {
      str += '\n' + field;

      (tags as any[]).forEach(tag => {
        Object.entries(metaToTags).forEach(([meta, metaTag]) => {
          if (tag === metaTag) {
            str += '\n   ' + meta;
          }
        });
      });

      return str;
    }, '') as string;

    this.elements = Object.values(metaToTags).reduce((str, tag: any) => {
      if (tag.el) {
        str += tag.el.outerHTML + '\n';
      }
      return str;
    }, '') as string;
  }
}
