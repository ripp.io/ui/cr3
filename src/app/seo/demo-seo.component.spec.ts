import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSeoComponent } from './demo-seo.component';

describe('DemoSeoComponent', () => {
  let component: DemoSeoComponent;
  let fixture: ComponentFixture<DemoSeoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoSeoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSeoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
