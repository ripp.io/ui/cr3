import { MetaMap } from '@cr3/seo/ng';

export const manualMap: MetaMap = {
  '/seo/manual': {
    title:       'Manual SEO Mapping',
    description: 'Example of SEO maintained as a manual map to centralize all SEO config'
  }
};
