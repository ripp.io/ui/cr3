import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoSeoRoutingModule } from './demo-seo-routing.module';
import { DemoSeoComponent } from './demo-seo.component';
import { DemoSeoDataComponent, DemoSeoManualComponent, DemoSeoStaticComponent } from './demo-seo-example.component';


@NgModule({
  declarations: [
    DemoSeoComponent,
    DemoSeoManualComponent,
    DemoSeoDataComponent,
    DemoSeoStaticComponent,
  ],
  imports: [
    CommonModule,
    DemoSeoRoutingModule
  ]
})
export class DemoSeoModule { }
