import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SeoModule } from '@cr3/seo/ng';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { manualMap } from './seo/demo-manual-map';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports:      [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserTransferStateModule,
    AppRoutingModule,
    SeoModule.withConfig({ manualMap }) // Example Manual Map SEO - see /seo/seo-manual
  ],
  providers:    [],
  bootstrap:    [AppComponent]
})
export class AppModule {}
