import { Component } from '@angular/core';

@Component({
  selector: 'demo-root',
  template: `
<header>
  <a routerLink="utils" routerLinkActive="active">Utils</a>
  <a routerLink="seo" routerLinkActive="active">SEO</a>
  <a routerLink="cache" routerLinkActive="active">Cache</a>
  <a routerLink="dynamic-children" routerLinkActive="active">Dynamic Children</a>
  <a routerLink="path-loader" routerLinkActive="active">Path Loader</a>
</header>
<div>
  <router-outlet></router-outlet>
</div>
<footer>
  <a href="https://ripp.io/cr3">ripp.io/cr3</a>
</footer>
`,
  styles:   []
})
export class AppComponent {
}

@Component({
  selector: 'demo-home',
  template: `<h2>This application is ugly!</h2>
  <p><b>TRUE</b>, this app is purely for developing, debugging, and demo purposes.
    <br>The least amount of styling and HTML is used to provide concise examples.</p>
  <p>This demo application may be referenced for implementation examples.</p>`,
  styles:   []
})
export class AppHomeComponent {
}
