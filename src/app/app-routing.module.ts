import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppHomeComponent } from './app.component';

const routes: Routes = [
  { path: '', component: AppHomeComponent },
  { path: 'seo', loadChildren: () => import('./seo/demo-seo.module').then(m => m.DemoSeoModule) },
  { path: 'dynamic-children', loadChildren: () => import('./dynamic-children/demo-dynamic-children.module').then(m => m.DemoDynamicChildrenModule) },
  { path: 'utils', loadChildren: () => import('./utils/demo-utils.module').then(m => m.DemoUtilsModule) },
  { path: 'cache', loadChildren: () => import('./cache/demo-cache.module').then(m => m.DemoCacheModule) },
  { path: 'path-loader', loadChildren: () => import('./path-loader/demo-path-loader.module').then(m => m.DemoPathLoaderModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
