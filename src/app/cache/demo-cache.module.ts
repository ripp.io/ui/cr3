import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoCacheRoutingModule } from './demo-cache-routing.module';
import { DemoCacheComponent } from './demo-cache.component';


@NgModule({
  declarations: [DemoCacheComponent],
  imports: [
    CommonModule,
    DemoCacheRoutingModule
  ]
})
export class DemoCacheModule { }
