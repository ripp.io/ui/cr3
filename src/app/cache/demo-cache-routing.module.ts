import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoCacheComponent } from './demo-cache.component';

const routes: Routes = [{ path: '', component: DemoCacheComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoCacheRoutingModule { }
