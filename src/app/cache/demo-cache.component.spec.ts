import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoCacheComponent } from './demo-cache.component';

describe('DemoCacheComponent', () => {
  let component: DemoCacheComponent;
  let fixture: ComponentFixture<DemoCacheComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoCacheComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoCacheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
