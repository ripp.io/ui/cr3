import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoPathLoaderComponent } from './demo-path-loader.component';

describe('DemoPathLoaderComponent', () => {
  let component: DemoPathLoaderComponent;
  let fixture: ComponentFixture<DemoPathLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoPathLoaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoPathLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
