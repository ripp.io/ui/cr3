import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoPathLoaderRoutingModule } from './demo-path-loader-routing.module';
import { DemoPathLoaderComponent } from './demo-path-loader.component';


@NgModule({
  declarations: [DemoPathLoaderComponent],
  imports: [
    CommonModule,
    DemoPathLoaderRoutingModule
  ]
})
export class DemoPathLoaderModule { }
