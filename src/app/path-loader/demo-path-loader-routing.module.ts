import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoPathLoaderComponent } from './demo-path-loader.component';

const routes: Routes = [{ path: '', component: DemoPathLoaderComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoPathLoaderRoutingModule { }
