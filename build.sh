#!/bin/sh

build() {
    npx ng build "${1}" --prod
}

build "@cr3/utils" &
build "@cr3/cache" &
wait

build "@cr3/seo" &
build "@cr3/path-loader" &
build "@cr3/dynamic-children" &
wait

echo Building Demo App:
build &
wait
