# @cr3 NPM Modules

See [ripp.io/cr3](https://ripp.io/cr3) for more friendly and interactive documentation.

Individual component README's can be found at:

* [Utils](./projects/cr3/utils/README.md)
  * TS/JS and framework utilities
* [Cache](./projects/cr3/cache/README.md)
  * Object Caching (in memory and/or across tabs/sessions)
* [SEO](./projects/cr3/seo/README.md)
  * Dynamic Search Engine Optimization
* [Dynamic Children](./projects/cr3/dynamic-children/README.md) (Angular Only)
  * Dynamically swap out `loadChildren` en-route
* [Path Loader](./projects/cr3/path-loader/README.md) (Angular Only)
  * Dynamically load components with data retrieved from a server
